package hackerrank.sorting;

public class BubbleSort {


    // https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting

    // Complete the countSwaps function below.
    static void countSwaps(int[] a) {

        int n = a.length;
        int swapCount = 0;

        for (int i = 0; i < n; i++) {

            for (int j = 0; j < n - 1; j++) {
                // Swap adjacent elements if they are in decreasing order
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    swapCount++;
                }
            }

        }

        System.out.println(String.format("Array is sorted in %d swaps.\nFirst Element: %d\nLast Element: %d", swapCount, a[0], a[n - 1]));
    }


    public static void main(String[] args){

    }

}
