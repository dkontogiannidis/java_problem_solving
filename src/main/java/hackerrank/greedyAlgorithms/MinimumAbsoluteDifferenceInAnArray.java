package hackerrank.greedyAlgorithms;

import java.util.Arrays;

public class MinimumAbsoluteDifferenceInAnArray {

    // https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

    // Complete the minimumAbsoluteDifference function below.
    static int minimumAbsoluteDifference(int[] arr) {

        Arrays.sort(arr);

        int minDiff = Integer.MAX_VALUE;

        for ( int i = 0; i < arr.length - 1; i++ ){
            int diff = Math.abs(arr[i] - arr[i+1]);
            minDiff = minDiff > diff ? diff : minDiff;
        }

        return minDiff;

    }
}
