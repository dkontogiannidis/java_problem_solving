package hackerrank.greedyAlgorithms;

import java.util.ArrayList;
import java.util.Collections;

public class LuckBalance {

    // https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

    // Complete the luckBalance function below.
    static int luckBalance(int k, int[][] contests) {

        int sumNotImportant = 0;

        ArrayList<Integer> important = new ArrayList<>();

        for (int i = 0; i < contests.length; i++) {

            if (contests[i][1] == 0)
                sumNotImportant += contests[i][0];
            else
                important.add(contests[i][0]);
        }

        important.sort(Collections.reverseOrder());

        return sumNotImportant +
                important.subList(0, k).stream().reduce(0, Integer::sum) -
                important.subList( k, important.size()).stream().reduce(0, Integer::sum);
    }

    public static void main(String[] args){

        int[][] contests = new int[6][2];
        contests[0] = new int[]{ 5, 1 };
        contests[1] = new int[]{ 2, 1 };
        contests[2] = new int[]{ 1, 1 };
        contests[3] = new int[]{ 8, 1 };
        contests[4] = new int[]{ 10, 0 };
        contests[5] = new int[]{ 5, 0 };

        System.out.println(luckBalance(2,contests ));
    }

}
