package hackerrank.stacksAndQueues;

/**
 * https://www.hackerrank.com/challenges/equal-stacks/problem
 */
public class EqualStacks {

    public static int equalStacks(int[] h1, int[] h2, int[] h3) {
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;

        for (int i = 0; i < Math.max(Math.max(h1.length, h2.length), h3.length); i++) {
            if (i < h1.length) {
                sum1 += h1[i];
            }
            if (i < h2.length) {
                sum2 += h2[i];
            }
            if (i < h3.length) {
                sum3 += h3[i];
            }
        }

        int min = Math.min(sum1, Math.min(sum2, sum3));

        if (min == 0) {
            return 0;
        }

        int i1 = 0;
        int i2 = 0;
        int i3 = 0;

        while (min > 0 && (sum1 != sum2 || sum2 != sum3)) {
            for (; i1 < h1.length && sum1 > min; i1++) {
                sum1 -= h1[i1];
            }

            min = Math.min(sum1, Math.min(sum2, sum3));

            for (; i2 < h2.length && sum2 > min; i2++) {
                sum2 -= h2[i2];
            }

            min = Math.min(sum1, Math.min(sum2, sum3));

            for (; i3 < h3.length && sum3 > min; i3++) {
                sum3 -= h3[i3];
            }

            min = Math.min(sum1, Math.min(sum2, sum3));
        }

        return Math.min(sum1, Math.min(sum2, sum3));
    }

}
