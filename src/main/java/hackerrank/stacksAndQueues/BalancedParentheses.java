package hackerrank.stacksAndQueues;

import helpers.TimeCheck;
import helpers.TimeChecked;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class BalancedParentheses {

    static String isBalanced(String s) {

        Stack<Character> stack = new Stack<>();

        if (s.length() == 0)
            return "YES";

        for ( char c : s.toCharArray() ){

            if ( isOpeningBracket(c) )
                stack.push(c);

            else if (stack.isEmpty() || !isBalanced(stack.pop(), c))
                return "NO";
        }

        return stack.isEmpty() ? "YES" : "NO";
    }

    private static boolean isBalanced(char oppening, char closing){
        return  ( (oppening == '{' && closing == '}') || (oppening == '(' && closing == ')' ) || (oppening == '[' && closing==']'));
    }

    private static boolean isOpeningBracket(char c){
        return Arrays.asList('(', '[', '{').contains(c);
    }


    public static void main(String[] args){

        List<TimeChecked<String>> operations = new ArrayList<>();

        operations.add(() -> isBalanced("]"));
        operations.add(() -> isBalanced("["));
        operations.add(() -> isBalanced("()"));
        operations.add(() -> isBalanced("[]"));
        operations.add(() -> isBalanced("{}"));
        operations.add(() -> isBalanced("{{}"));
        operations.add(() -> isBalanced("{[)}"));
        operations.add(() -> isBalanced("{[()]}"));
        operations.add(() -> isBalanced("{[()]}{"));
        operations.add(() -> isBalanced("{[(()]}"));
        operations.add(() -> isBalanced("{[())]}"));
        operations.add(() -> isBalanced("{[((({})))]}"));

        TimeCheck.executeBatch(operations);

    }
}
