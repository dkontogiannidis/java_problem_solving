package hackerrank.hashmaps;

import java.util.*;

public class CountTriplets {

    // Complete the countTriplets function below.
    static long countTriplets(List<Long> arr, long r) {

        long count = 0;

        HashMap<Long, Long> partTwo = new HashMap<>();
        HashMap<Long, Long> partThree = new HashMap<>();

        for ( long num : arr ){
            if ( partThree.containsKey(num) )
                count += partThree.get(num);

            if (partTwo.containsKey(num))
                partThree.merge(num * r, partTwo.get(num), Long::sum);

            partTwo.merge( num * r, 1L, Long::sum );
        }
        return count;

    }

    public static void main(String[] args){

        List<Long> lst = Arrays.asList( 1L, 5L, 5L, 25L, 125L );
        List<Long> lst2 = Arrays.asList( 1L, 2L, 1L, 2L, 4L );

        System.out.println(countTriplets(lst, 5L));
        System.out.println(countTriplets(lst2, 2L));
    }

}
