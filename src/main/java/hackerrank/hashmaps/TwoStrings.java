package hackerrank.hashmaps;

import java.util.HashSet;
import java.util.stream.Collectors;

public class TwoStrings {

    // https://www.hackerrank.com/challenges/two-strings/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps

    // Complete the twoStrings function below.
    static String twoStrings(String s1, String s2) {

        HashSet<Character> set1 = new HashSet<Character>();
        for ( char c : s1.toCharArray() ){
            set1.add(c);
        }

        int lenBefore = set1.size();

        set1.removeAll(s2.replace(" ", "").chars().mapToObj(c -> (char) c).collect(Collectors.toList()));

        return lenBefore != set1.size() ? "YES" : "NO";
    }
}
