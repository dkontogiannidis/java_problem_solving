package hackerrank.hashmaps;

import java.util.HashMap;

public class RansomNote {

    // https://www.hackerrank.com/challenges/ctci-ransom-note/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps

    static void checkMagazine(String[] magazine, String[] note){

        HashMap<String, Integer> wordMap = new HashMap<>();

        for ( String word: magazine ){
            wordMap.putIfAbsent(word, 0);
            wordMap.compute(word, (x,y) -> y + 1);
        }

        for ( String word : note){
            if ( !wordMap.containsKey(word) || wordMap.get(word) == 0 ){
                System.out.println("No");
                return;
            }
            wordMap.compute(word, (x,y) -> y - 1);
        }

        System.out.println("Yes");

    }

}
