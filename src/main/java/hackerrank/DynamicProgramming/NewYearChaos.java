package hackerrank.DynamicProgramming;

import java.util.Arrays;

import static java.lang.Integer.max;

public class NewYearChaos {

    static void minimumBribes(int[] q) {

        int[] memory = new int[q.length + 1];


        for ( int i = 0; i < q.length; i++ ) {

            for ( int j = i; j < q.length ; j++ ) {

                if ( q[i] <= q[j] )
                    continue;

                memory[q[i]] += 1;

                int temp = q[i];
                q[i] = q[j];
                q[j] = temp;
            }
        }

        if ( Arrays.stream(memory).anyMatch( x -> x > 2 ) )
            System.out.println("Too chaotic");
        else
            System.out.println( Arrays.stream(memory).reduce(0, Integer::sum) );
    }

    static void minimumBribesImproved(int[] q)
    {
        int bribes = 0;
        for (int i = q.length - 1; i >= 0; i--) {
            if (q[i] - i - 1 > 2) {
                System.out.println("Too Chaotic");
                return;
            }
            for (int j = max(0, q[i] - 2); j < i; j++)
                if (q[j] > q[i]) bribes++;
        }
        System.out.println(bribes);
    }

    static void printer( int[] q ){
        Arrays.stream(q).forEach( x -> System.out.print(x + " ") );
        System.out.println("  ");
    }

    public static void main(String[] args) {
        minimumBribes( new int[]{2, 1, 5, 3, 4} );
        minimumBribes( new int[]{2,5,1,3,4} );
        minimumBribes( new int[]{5, 1, 2, 3, 7, 8, 6, 4} );
        minimumBribesImproved( new int[]{2, 1, 5, 3, 4} );
        minimumBribesImproved( new int[]{2,5,1,3,4} );
        minimumBribesImproved( new int[]{5, 1, 2, 3, 7, 8, 6, 4} );
    }
}
