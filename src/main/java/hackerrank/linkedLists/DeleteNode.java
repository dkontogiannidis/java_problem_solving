package hackerrank.linkedLists;

import helpers.list.LinkedListPrinter;
import helpers.list.LinkedListUtils;
import helpers.list.SinglyLinkedListNode;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list/problem
 */
public class DeleteNode {

    static SinglyLinkedListNode deleteNode(SinglyLinkedListNode head, int position) {
        return LinkedListUtils.deleteNode(head, position);
    }

    public static void run(List<Integer> input){
        SinglyLinkedListNode head = new SinglyLinkedListNode(input.get(0));
        for (int i = 1; i < input.size() - 1; i++) {
            head = LinkedListUtils.insertNodeAtTail(head, input.get(i));
        }
        head = deleteNode(head, input.get(input.size() - 1 ));
        LinkedListPrinter.print(head, " ");
    }

}
