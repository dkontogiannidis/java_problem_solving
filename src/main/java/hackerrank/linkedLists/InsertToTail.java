package hackerrank.linkedLists;

import helpers.list.LinkedListPrinter;
import helpers.list.LinkedListUtils;
import helpers.list.SinglyLinkedListNode;

/**
 * Insert a Node at the Tail of a Linked List
 * https://www.hackerrank.com/challenges/insert-a-node-at-the-tail-of-a-linked-list/problem
 */
public class InsertToTail {


    private static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {
        return LinkedListUtils.insertNodeAtTail(head, data);
    }

    public static void main(String[] args) {
        SinglyLinkedListNode head = insertNodeAtTail(null, 12);
        head = insertNodeAtTail(head, 15);
        head = insertNodeAtTail(head, 16);
        head = insertNodeAtTail(head, 17);
        head = insertNodeAtTail(head, 18);
        LinkedListPrinter.print(head, "->");
    }

}
