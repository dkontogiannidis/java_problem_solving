package hackerrank.linkedLists;

import helpers.list.LinkedListPrinter;
import helpers.list.SinglyLinkedListNode;

import java.util.List;

public class InsertAtHeadLl {

    static SinglyLinkedListNode insertNodeAtHead(SinglyLinkedListNode llist, int data) {
        if (llist == null) {
            return new SinglyLinkedListNode(data);
        }

        SinglyLinkedListNode node = new SinglyLinkedListNode(data);
        node.next = llist;
        return node;
    }

    public static void run(List<String> input){
        SinglyLinkedListNode head = null;
        for (String line: input) {
            head = insertNodeAtHead(head, Integer.parseInt(line));
        }
        LinkedListPrinter.print(head, "\n");
    }

}
