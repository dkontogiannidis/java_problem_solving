package hackerrank.trees;

import helpers.tree.Tree.Node;

public class TreeHeight {


    public static int height(Node root) {

        return root == null ? 0 : getHeight(root) - 1;
    }

    private static int getHeight(Node node){

        if (node==null) return 0;

        int leftHeight = getHeight(node.left);
        int rightHeight = getHeight(node.right);

        return Math.max(leftHeight, rightHeight) + 1;
    }

    public static void main(String[] args){

    }

}
