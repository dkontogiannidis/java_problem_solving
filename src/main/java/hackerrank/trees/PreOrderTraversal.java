package hackerrank.trees;

import helpers.tree.Tree.Node;

public class PreOrderTraversal {

    public static void preOrder(Node root) {

        if (root==null)
            return;

        System.out.print(root.data + " ");
        preOrder(root.left);
        preOrder(root.right);
    }
}
