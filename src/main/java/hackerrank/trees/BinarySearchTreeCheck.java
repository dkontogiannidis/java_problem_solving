package hackerrank.trees;

import helpers.tree.Tree.*;

public class BinarySearchTreeCheck {

    //  https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=trees

    boolean checkBST(Node root) {

        return checkBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    boolean checkBST( Node node, int left, int right ){

        return node == null ||
                (left < node.data && right > node.data &&
                checkBST(node.left, left, node.data) && checkBST(node.right, node.data, right));
    }

}
