package hackerrank.trees;

import helpers.tree.Tree.*;

public class TreeTopView {

    //todo: NOT SOLVED

    public static void topView(Node root) {
        topView(root, 0);
    }

    public static void topView(Node node, int side)
    {
        if ( node != null )
        {
            if ( side <= 0)
                topView(node.left, -1);

            System.out.print(node.data + " ");

            if ( side >= 0 )
                topView(node.right, 1);
        }
    }

}
