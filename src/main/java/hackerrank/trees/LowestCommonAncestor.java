package hackerrank.trees;

import helpers.TimeCheck;
import helpers.TimeChecked;
import helpers.tree.TestTree;
import helpers.tree.Tree.Node;

import java.util.ArrayList;
import java.util.List;

public class LowestCommonAncestor {


    public static Node lca(Node root, int v1, int v2) {

        if (root == null)
            return null;

        if (root.data < v1 && root.data < v2)
            return lca(root.right, v1, v2);

        if (root.data > v1 && root.data > v2)
            return lca(root.left, v1, v2);

        return root;

    }

    public static void main(String[] args) {

        List<TimeChecked<Node>> operations = new ArrayList<>();

        Node node = TestTree.getBinaryTree();

        operations.add(() -> lca(node, 1, 7));

        TimeCheck.executeBatch(operations);

    }

}
