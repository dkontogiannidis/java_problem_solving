package hackerrank.stringManipulation;

import helpers.TimeCheck;
import helpers.TimeChecked;
import helpers.array.TwoDimensionArrayPrinter;

import java.util.ArrayList;
import java.util.List;

public class LongestCommonSubsequence {

    static int commonChild(String s1, String s2) {

        int row = s1.length()+1;
        int col = s2.length()+1;

        Integer[][] arr = new Integer[row][col];

        for (int j = 0; j < col; j++){
            arr[0][j] = 0;
        }

        for (int i=0; i < row; i++){
            arr[i][0] = 0;
        }

        for (int i=1; i < row; i++){
            for (int j = 1; j < col; j++){

                if ( s1.charAt(i-1) == s2.charAt(j-1) ){
                    arr[i][j] = arr[i-1][j-1] + 1;
                }
                else {
                    arr[i][j] = Math.max(arr[i][j-1], arr[i-1][j]);
                }
            }
        }
        TwoDimensionArrayPrinter.printArray(arr);
        return arr[row-1][col-1];
    }

    public static void main(String[] args){

        List<TimeChecked<Integer>> operations = new ArrayList<>();

        operations.add(() -> commonChild("NOHARAAA", "SHINCHAN"));

        TimeCheck.executeBatch(operations);

    }

}
