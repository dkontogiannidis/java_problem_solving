package hackerrank.stringManipulation;

import helpers.TimeCheck;
import helpers.TimeChecked;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SherlocAndValidString {

    // Complete the isValid function below.
    static String isValid(String s) {

        if (s == null || s.equals(""))
            return "YES";

        HashMap<Character, Integer> charMap = new HashMap<>();
        HashMap<Integer, Integer> count = new HashMap<>();

        for (char c : s.toCharArray()) {
            charMap.putIfAbsent(c, 0);
            charMap.put(c, charMap.get(c) + 1);
        }

        for (Integer val : charMap.values()) {
            count.putIfAbsent(val, 0);
            count.put(val, count.get(val) + 1);
        }

        if (count.values().size() == 1)
            return "YES";
        else if (count.values().size() > 2)
            return "NO";

        ArrayList<Integer> countLst = new ArrayList<>(count.keySet());

        int min = Math.min(countLst.get(0), countLst.get(1));
        int max = Math.max(countLst.get(0), countLst.get(1));

        if ( (max - min == 1 || min == 1) && count.get(min) == 1 )
            return "YES";

        if ( max - min == 1 && count.get(max) == 1)
            return "YES";

        return "NO";

    }

    public static void main(String[] args) {
        List<TimeChecked<String>> operations = new ArrayList<>();
        operations.add(() -> isValid("aaaabbcc"));
        operations.add(() -> isValid("aabbcd"));
        operations.add(() -> isValid("ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd"));
        operations.add(() -> isValid("abcdefghhgfedecba"));
        operations.add(() -> isValid("aaaaabc"));
        TimeCheck.executeBatch(operations);
    }

}
