package hackerrank.stringManipulation;

public class AlternatingStrings {

    // https://www.hackerrank.com/challenges/alternating-characters/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings

    // Complete the alternatingCharacters function below.
    static int alternatingCharacters(String s) {

        char[] str = s.toCharArray();
        int removals = 0;
        for ( int i = 0; i < str.length-1; i++ ){

            if (str[i] == str[i+1]){
                removals++;
            }
        }
        return removals;
    }

    public static void main(String[] args){

        System.out.println(alternatingCharacters("AABBBBBABBAAA"));

    }
}
