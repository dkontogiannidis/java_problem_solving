package hackerrank.stringManipulation;

import java.util.HashMap;
import java.util.HashSet;

public class MakingAnagrams {

    // https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings

    // Complete the makeAnagram function below.
    static int makeAnagram(String a, String b) {

        HashMap<Character, Integer> mapA = new HashMap<>();
        HashMap<Character, Integer> mapB = new HashMap<>();

        int diff = 0;

        for ( char c : a.toCharArray() ){
            mapA.merge(c, 1, Integer::sum);
        }

        for ( char c : b.toCharArray() ){
            mapB.merge(c, 1, Integer::sum);
        }

        HashSet<Character> keys = new HashSet<>();
        keys.addAll(mapA.keySet());
        keys.addAll(mapB.keySet());

        for ( char c : keys ){
            diff += Math.abs( mapA.getOrDefault(c, 0) - mapB.getOrDefault(c, 0) );
        }

        return diff;
    }

    // Complete the makeAnagram function below.
    static int makeAnagramArraySolution(String a, String b) {

        int[] countA = new int[26];
        int[] countB = new int[26];

        int diff = 0;

        for ( int c : a.toCharArray() ){
            countA[c - 97]++;
        }

        for ( char c : b.toCharArray() ){
            countB[c-97]++;
        }

        for ( int i = 0; i < 26; i++ ){
            diff += Math.abs( countA[i] - countB[i]);
        }

        return diff;
    }

    public static void main(String[] args){

        System.out.println(makeAnagram("abcc", "cdee"));
        System.out.println(makeAnagramArraySolution("abcc", "cdee"));

    }

}
