package hackerrank.stringManipulation;

import helpers.TimeCheck;
import helpers.TimeChecked;
import helpers.string.EncodedChar;
import helpers.string.RunLengthEncoding;

import java.util.ArrayList;
import java.util.List;

public class SpecialPalindrome {


    static long substrCount(int n, String s) {

        List<EncodedChar> chars = RunLengthEncoding.encodeString(s);

        long count = 0;

        for (int i = 0; i < chars.size(); i++) {

            for (int j = chars.get(i).getCount(); j > 0; j--) {
                count += j;
            }

            if (chars.size() > i + 2 && chars.get(i + 1).getCount() <= 1 && chars.get(i).getCharacter() == chars.get(i + 2).getCharacter()) {
                count += Math.min(chars.get(i + 2).getCount(), chars.get(i).getCount());
            }
        }

        return count;
    }


    public static void main(String[] args) {

        List<TimeChecked<Long>> operations = new ArrayList<>();

        // 7
        operations.add(() -> substrCount(5, "asasd"));
        // 10
        operations.add(() -> substrCount(7, "abcbaba"));
        // 10
        operations.add(() -> substrCount(4, "aaaa"));
        // 12
        operations.add(() -> substrCount(8, "mnonopoo"));
        // 9
        operations.add(() -> substrCount(5, "aabaa"));
        // 9
        operations.add(() -> substrCount(6, "aabbaa"));

        TimeCheck.executeBatch(operations);

    }

}
