package hackerrank.arrays;

public class ArrayManipulation {

    // https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

    static long arrayManipulation(int n, int[][] queries) {

        if (n==0)
            return 0;

        long[] arr = new long[n+1];

        // find the difference between the current element and the previous one after the operation
        for (int[] query : queries) {
            arr[query[0] - 1] = arr[query[0] - 1] + query[2];
            arr[query[1]] = arr[query[1]] - query[2];
        }

        long max = 0L;
        long current = 0L;
        for (long l : arr) {
            current += l;
            max = current > max ? current : max;
        }

        return max;
    }
}
