package hackerrank.arrays;

import helpers.TimeCheck;
import helpers.TimeChecked;

import java.util.ArrayList;
import java.util.List;

public class ReverseArray {

    static int[] reverseArray(int[] a) {

        for (int i = 0; i < a.length/2; i++){
            int temp = a[i];
            a[i] = a[a.length - i - 1];
            a[a.length - i - 1] = temp;
        }

        return a;
    }

    public static void main(String[] args){

        List<TimeChecked<int[]>> operations = new ArrayList<>();

        operations.add(() -> reverseArray(new int[]{1, 2, 3, 4}));
        operations.add(() -> reverseArray(new int[]{1, 2, 3, 4, 5}));

        TimeCheck.executeBatch(operations);

    }

}
