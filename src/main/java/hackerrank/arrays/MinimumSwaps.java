package hackerrank.arrays;

import java.util.Arrays;

public class MinimumSwaps {

    // https://www.hackerrank.com/challenges/minimum-swaps-2/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

    // Complete the minimumSwaps function below.
    static int minimumSwaps(int[] arr) {

        int swaps = 0;

        for (int i = 0; i < arr.length; i++){

            if ( arr[i] == i + 1 ){
                continue;
            }

            int temp = arr[i];
            arr[i] = arr[temp - 1];
            arr[temp - 1] = temp;

            swaps++;
            i--;

            System.out.println(Arrays.toString(arr));

        }

        return swaps;
    }

    public static void main(String[] args){

        int arr[] = new int[]{ 1, 3, 5, 2, 4, 6, 7 };

        System.out.println(minimumSwaps(arr));

    }
}
