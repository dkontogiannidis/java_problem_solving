package hackerrank.arrays;

import helpers.TimeCheck;
import helpers.TimeChecked;

import java.util.ArrayList;
import java.util.List;

public class ArrayLeftRotation {

    static int[] rotLeft(int[] a, int d) {
        for (int r = 0; r < d; r++){
            int temp = a[0];
            int i = 0;
            for (i = 0; i < a.length - 1; i++){
                a[i] = a[i+1];
            }
            a[i] = temp;
        }
        return a;
    }

    public static void main(String[] args){

        List<TimeChecked<int[]>> operations = new ArrayList<>();

        operations.add(() -> rotLeft(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3));

        TimeCheck.executeBatch(operations);

    }

}
