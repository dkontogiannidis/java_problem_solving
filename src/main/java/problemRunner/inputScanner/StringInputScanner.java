package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class StringInputScanner<T> extends AbstractInputScanner<String> {


    public StringInputScanner(Class classUnderTest, File[] files) {
        super(classUnderTest, files);
    }

    @Override
    protected void addLine(String line, FileInput<String> fileInput) {
        if (StringUtils.isNotEmpty(line)) {
            fileInput.add(line);
        }
    }
}
