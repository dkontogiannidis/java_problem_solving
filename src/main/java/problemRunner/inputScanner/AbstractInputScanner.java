package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;
import problemRunner.inputFile.InputFileException;
import problemRunner.util.OutputHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public abstract class AbstractInputScanner<T> implements InputScanner<T> {

    private File[] files;
    private Class classUnderTest;

    protected AbstractInputScanner(Class classUnderTest, File[] files) {
        this.files = files;
        this.classUnderTest = classUnderTest;
    }

    protected File[] getFiles() {
        return files;
    }

    protected abstract void addLine(String line, FileInput<T> fileInput);

    @Override
    public List<FileInput<T>> getImportedFiles() {
        List<FileInput<T>> input = new ArrayList<>();
        try {
            Map<String, String> expectedOutput = OutputHandler.getExpectedOutput(getFiles());
            for (File file: getFiles()) {
                FileInput<T> lines = new FileInput<>(classUnderTest, file.getName());
                lines.setExpectedResult(expectedOutput.get(file.getName()));
                Scanner scanner = new Scanner(file);
                scanner.nextLine();
                while (scanner.hasNextLine()) {
                    addLine(scanner.nextLine(), lines);
                }
                input.add(lines);
            }
        }
        catch (Exception e) {
            throw new InputFileException(e);
        }

        return input;
    }
}
