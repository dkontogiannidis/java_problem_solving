package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;
import helpers.list.SinglyLinkedListNode;

import java.io.File;

public class SinglyLinkedListNodeInputScanner<T> extends AbstractInputScanner<SinglyLinkedListNode> {


    public SinglyLinkedListNodeInputScanner(Class classUnderTest, File[] files) {
        super(classUnderTest, files);
    }

    @Override
    protected void addLine(String line, FileInput<SinglyLinkedListNode> fileInput) {
        fileInput.add(new SinglyLinkedListNode(Integer.parseInt(line)));
    }

}
