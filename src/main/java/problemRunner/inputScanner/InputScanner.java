package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;

import java.util.List;

public interface InputScanner<T> {

    List<FileInput<T>> getImportedFiles();

}
