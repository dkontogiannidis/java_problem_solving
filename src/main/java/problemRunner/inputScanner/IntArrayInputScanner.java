package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;

import java.io.File;
import java.util.*;

public class IntArrayInputScanner<T> extends AbstractInputScanner<int[]> {

    public IntArrayInputScanner(Class classUnderTest, File[] files) {
        super(classUnderTest, files);
    }

    @Override
    protected void addLine(String line, FileInput<int[]> fileInput) {
        fileInput.add(Arrays.stream(line.split(" ")).mapToInt(Integer::valueOf).toArray());
    }
}
