package problemRunner.inputScanner;

import problemRunner.inputFile.FileInput;

import java.io.File;

public class IntInputScanner<T> extends AbstractInputScanner<Integer> {


    public IntInputScanner(Class classUnderTest, File[] files) {
        super(classUnderTest, files);
    }

    @Override
    protected void addLine(String line, FileInput<Integer> fileInput) {
        fileInput.add(Integer.parseInt(line));
    }
}
