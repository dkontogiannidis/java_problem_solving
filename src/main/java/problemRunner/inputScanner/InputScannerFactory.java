package problemRunner.inputScanner;

import problemRunner.inputFile.InputFileResolver;
import helpers.list.SinglyLinkedListNode;

import java.io.File;

public class InputScannerFactory {

    public static <T> InputScanner<T> getInputProcessor(Class cls, T t) {
        File[] inputFiles = InputFileResolver.resolveFiles(cls);

        if (t instanceof int[]) {
            return new IntArrayInputScanner(cls, inputFiles);
        }
        else if (t instanceof SinglyLinkedListNode) {
            return new SinglyLinkedListNodeInputScanner(cls, inputFiles);
        }
        else if (t instanceof String) {
            return new StringInputScanner(cls, inputFiles);
        }
        else if (t instanceof Integer) {
            return new IntInputScanner(cls, inputFiles);
        }

        throw new RuntimeException("No scanner found for type: " + t.getClass().getName());
    }

}
