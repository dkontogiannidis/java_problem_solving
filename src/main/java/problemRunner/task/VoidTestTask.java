package problemRunner.task;

import problemRunner.result.ResultHandlerSingleton;
import problemRunner.inputFile.FileInput;
import problemRunner.util.ThreadPrintStream;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class VoidTestTask<T> implements Runnable {

    private final FileInput<T> fileInput;
    private final VoidFunctionRunner<T> voidFunctionRunner;

    public VoidTestTask(FileInput<T> fileInput, VoidFunctionRunner<T> voidFunctionRunner) {
        this.fileInput = fileInput;
        this.voidFunctionRunner = voidFunctionRunner;
    }

    @Override
    public void run() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(out);
            // Install the PrintStream to be used as System.out for this thread.
            ((ThreadPrintStream) System.out).setThreadOut(stream);
            long startTime = System.nanoTime();
            voidFunctionRunner.run(fileInput);
            long endTime = System.nanoTime();
            BigDecimal elapsedTime = BigDecimal.valueOf(endTime).subtract(BigDecimal.valueOf(startTime))
                    .setScale(4, RoundingMode.HALF_EVEN)
                    .divide(BigDecimal.valueOf(1000000), RoundingMode.HALF_EVEN);
            ResultHandlerSingleton.getInstance().writeResult(fileInput, out.toString(), elapsedTime.toString());
            System.out.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public interface VoidFunctionRunner<T> {
        void run(FileInput<T> f);
    }
}