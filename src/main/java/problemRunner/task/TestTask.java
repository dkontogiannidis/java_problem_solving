package problemRunner.task;

import problemRunner.result.ResultHandlerSingleton;
import problemRunner.inputFile.FileInput;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TestTask<T> implements Runnable {

    private final FileInput<T> fileInput;
    private final FunctionRunner<T> functionRunner;

    public TestTask(FileInput<T> fileInput, FunctionRunner<T> functionRunner) {
        this.fileInput = fileInput;
        this.functionRunner = functionRunner;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        String out = functionRunner.run(fileInput);
        long endTime = System.nanoTime();
        BigDecimal elapsedTime = BigDecimal.valueOf(endTime).subtract(BigDecimal.valueOf(startTime))
                .setScale(4, RoundingMode.HALF_EVEN)
                .divide(BigDecimal.valueOf(1000000), RoundingMode.HALF_EVEN);
        ResultHandlerSingleton.getInstance().writeResult(fileInput, out, elapsedTime.toString());
    }

    public interface FunctionRunner<T> {
        String run(FileInput<T> f);
    }

}
