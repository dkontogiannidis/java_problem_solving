package problemRunner.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class OutputHandler {

    public static Map<String, String> getExpectedOutput(File[] files) throws IOException, ParseException {
        if (files.length == 0)
            return Collections.emptyMap();
        File jsonFile = new File(OutputHandler.class.getClassLoader().getResource("expectedResults.json").getFile());
        JSONParser parser = new JSONParser();
        JSONObject object = (JSONObject) parser.parse(new FileReader(jsonFile));
        for(String path : files[0].getPath().split("resources\\\\main\\\\")[1].split("\\\\")){
            if (!path.contains("."))
                object = (JSONObject) object.get(path);
        }
        final JSONObject dir = object;
        return Arrays.stream(files).collect(Collectors.toMap(File::getName, x -> (String) dir.getOrDefault(x.getName(), "?")));
    }

}
