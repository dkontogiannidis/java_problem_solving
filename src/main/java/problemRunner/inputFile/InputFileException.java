package problemRunner.inputFile;

public class InputFileException extends RuntimeException {

    public InputFileException(String message) {
        super(message);
    }

    public InputFileException(Throwable cause) {
        super(cause);
    }
}
