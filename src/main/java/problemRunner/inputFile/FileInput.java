package problemRunner.inputFile;

import java.util.ArrayList;
import java.util.List;

public class FileInput<T> {

    private String fileName;
    private String expectedResult;
    private Class classUnderTest;
    private List<T> lines;

    public FileInput(Class classUnderTest, String fileName) {
        this.fileName = fileName;
        this.lines = new ArrayList<>();
        this.expectedResult = "?";
        this.classUnderTest = classUnderTest;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<T> getLines() {
        return lines;
    }

    public void add(T line) {
        this.lines.add(line);
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public Class getClassUnderTest() {
        return classUnderTest;
    }
}
