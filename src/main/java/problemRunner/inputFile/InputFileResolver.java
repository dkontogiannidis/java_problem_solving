package problemRunner.inputFile;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class InputFileResolver {

    public static File[] resolveFiles(Class cls) throws InputFileException {
        File file = new File(Objects.requireNonNull(cls.getClassLoader().getResource(resolvePath(cls) + cls.getSimpleName())).getFile());
        if (!file.exists())
            throw new InputFileException("Path '" + file.getAbsolutePath() + "' does not exist.");
        return Arrays.stream(Objects.requireNonNull(file.listFiles())).filter(File::isFile).toArray(File[]::new);
    }

    private static String resolvePath(Class cls) {
        return cls.getResource("").getPath().split("java/main/")[1];
    }

}
