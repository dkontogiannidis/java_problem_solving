package problemRunner.result;

public class TestResult {

    private final String actualResult;
    private final String expectedResult;
    private final String elapsedTimeMSec;

    public TestResult(String actualResult, String expectedResult, String elapsedTimeMSec) {
        this.actualResult = actualResult;
        this.expectedResult = expectedResult;
        this.elapsedTimeMSec = elapsedTimeMSec;
    }

    public String getActualResult() {
        return actualResult;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public String getElapsedTimeMSec() {
        return elapsedTimeMSec;
    }

    public boolean isPassed() {
        return expectedResult.equals(actualResult);
    }
}
