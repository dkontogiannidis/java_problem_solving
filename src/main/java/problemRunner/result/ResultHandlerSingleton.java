package problemRunner.result;

import problemRunner.inputFile.FileInput;

import java.util.concurrent.ConcurrentHashMap;

public class ResultHandlerSingleton {

    private final ConcurrentHashMap<Class, ConcurrentHashMap<String, TestResult>> passedTestMap;
    private final ConcurrentHashMap<Class, ConcurrentHashMap<String, TestResult>> failedTestMap;
    private static ResultHandlerSingleton INSTANCE;

    private ResultHandlerSingleton() {
        this.passedTestMap = new ConcurrentHashMap<>();
        this.failedTestMap = new ConcurrentHashMap<>();
    }

    public static ResultHandlerSingleton getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ResultHandlerSingleton();
        return INSTANCE;
    }

    public <T> void writeResult(FileInput<T> fileInput, String actual, String elapsedTimeMSec) {
        writeResult(fileInput, new TestResult(actual, fileInput.getExpectedResult(), elapsedTimeMSec));
    }

    public <T> void writeResult(FileInput<T> fileInput, TestResult result) {
        if (result.isPassed()) {
            passedTestMap.putIfAbsent(fileInput.getClassUnderTest(), new ConcurrentHashMap<>());
            passedTestMap.get(fileInput.getClassUnderTest()).put(fileInput.getFileName(), result);
        } else {
            failedTestMap.putIfAbsent(fileInput.getClassUnderTest(), new ConcurrentHashMap<>());
            failedTestMap.get(fileInput.getClassUnderTest()).put(fileInput.getFileName(), result);
        }
    }

    public void printResults() {
        StringBuilder out = new StringBuilder();
        if (!passedTestMap.isEmpty()) {
            out.append("\nPASSED:\n");
            printResults(out, passedTestMap);
        }
        if (!failedTestMap.isEmpty()) {
            out.append("\nFAILED:\n");
            printResults(out, failedTestMap);
        }
        System.out.print(out);
    }

    private void printResults(StringBuilder out, ConcurrentHashMap<Class, ConcurrentHashMap<String, TestResult>> results) {
        for (ConcurrentHashMap.Entry<Class, ConcurrentHashMap<String, TestResult>> entry : results.entrySet()) {
            out.append("    ").append(entry.getKey().getName()).append("\n");
            for (ConcurrentHashMap.Entry<String, TestResult> result : entry.getValue().entrySet()) {
                printResult(out, result.getKey(), result.getValue());
            }
        }
    }

    private void printResult(StringBuilder out, String caseInput, TestResult result) {
        out.append("        ").append("Input: ").append(caseInput).append(" -> ")
                .append("Elapsed Time: ").append(result.getElapsedTimeMSec()).append("ms")
                .append(" ,Expected Result: ").append(result.getExpectedResult().replace("\n", "\\n"))
                .append(" ,Actual Result: ").append(result.getActualResult().replace("\n", "\\n")).append("\n");
    }

    public void reset() {
        INSTANCE = null;
    }


}
