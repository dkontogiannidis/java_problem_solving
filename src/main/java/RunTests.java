import hackerrank.linkedLists.DeleteNode;
import hackerrank.linkedLists.InsertAtHeadLl;
import hackerrank.stacksAndQueues.EqualStacks;
import problemRunner.inputFile.FileInput;
import problemRunner.inputScanner.InputScannerFactory;
import problemRunner.result.ResultHandlerSingleton;
import problemRunner.task.TestTask;
import problemRunner.task.VoidTestTask;
import problemRunner.util.ThreadPrintStream;

import java.util.ArrayList;
import java.util.List;

public class RunTests {

    private static List<Thread> threads = new ArrayList<>();

    private static void runTests() {

        // Stacks and Queues
        runTest(EqualStacks.class, new int[]{},
                (f) -> Integer.toString(EqualStacks.equalStacks(f.getLines().get(0), f.getLines().get(1), f.getLines().get(2))));

        // Linked Lists
        runVoidTest(InsertAtHeadLl.class, "", (f) -> InsertAtHeadLl.run(f.getLines()));
        runVoidTest(DeleteNode.class, 0, (f) -> DeleteNode.run(f.getLines()));
    }

    private static <T> void runVoidTest(Class cls, T t, VoidTestTask.VoidFunctionRunner<T> voidFunctionRunner) {
        for (FileInput<T> fileInput : getImportedFiles(cls, t)) {
            Thread thread = new Thread(new VoidTestTask<>(fileInput, voidFunctionRunner));
            threads.add(thread);
            thread.start();
        }
    }

    private static <T> void runTest(Class cls, T t, TestTask.FunctionRunner<T> functionRunner) {
        for (FileInput<T> fileInput : getImportedFiles(cls, t)) {
            Thread thread = new Thread(new TestTask<>(fileInput, functionRunner));
            threads.add(thread);
            thread.start();
        }
    }

    private static <T> List<FileInput<T>> getImportedFiles(Class cls, T t) {
        return InputScannerFactory.getInputProcessor(cls, t).getImportedFiles();
    }

    public static void main(String[] args) {
        ThreadPrintStream.replaceSystemOut();
        System.out.println("\n*******************\n** Running Tests **\n*******************\n");
        runTests();

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        ResultHandlerSingleton.getInstance().printResults();
    }

}
