package helpers;

public interface TimeChecked<T> {
    T run();
}

