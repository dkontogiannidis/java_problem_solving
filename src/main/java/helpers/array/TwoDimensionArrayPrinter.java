package helpers.array;

import java.util.Arrays;
import java.util.Comparator;

public class TwoDimensionArrayPrinter {

    public static <T> void printArray(T[][] arr){

        for (T[] ts : arr) {
            printSeparator(ts.length - 1);
            StringBuilder sb = new StringBuilder().append("* ");
            for (T t : ts) {
                sb.append(t).append(" ");
            }
            System.out.println(sb.append("*").toString());
        }
        printSeparator(arr[0].length - 1);
    }

    private static void printSeparator(int size){
        StringBuilder sb = new StringBuilder().append("**");
        for (int i = 0; i <= size; i++){
            sb.append("**");
        }
        System.out.println(sb.append("*").toString());
    }

}
