package helpers.array;

import java.util.StringJoiner;

public class ArrayPrinter {

    public static <T> void print(T[] arr) {

        StringJoiner sj = new StringJoiner(", ");
        for (T val : arr) {
            sj.add(getPrintable(val));
        }
        System.out.print("{ " + sj.toString() + " }\n");
    }

    private static <T> String getPrintable(T val) {

        if (val == null)
            return "<null>";

        if (val instanceof Integer)
            return Integer.toString((Integer) val);

        if (val instanceof String)
            return (String)val;

        if (val instanceof Double)
            return Double.toString((Double) val);

        if (val instanceof Short)
            return Short.toString((Short) val);

        if (val instanceof Long)
            return Long.toString((Long) val);

        return val.toString();
    }

}
