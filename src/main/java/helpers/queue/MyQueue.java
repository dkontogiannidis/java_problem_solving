package helpers.queue;

public interface MyQueue<T> {

    void enqueue(T val);

    T dequeue();

    T peek();
}
