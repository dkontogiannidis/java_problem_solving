package helpers.queue;

import java.util.Stack;

public class StackedQueue<T> implements MyQueue<T> {

    private Stack<T> inStack;
    private Stack<T> outStack;

    public StackedQueue() {
        this.inStack = new Stack<>();
        this.outStack = new Stack<>();
    }

    @Override
    public void enqueue(T val) {
        inStack.push(val);
    }

    @Override
    public T dequeue() {
        handleStacks();
        return outStack.pop();
    }

    @Override
    public T peek() {
        handleStacks();
        return outStack.peek();
    }

    private void handleStacks() {
        if (!outStack.isEmpty())
            return;
        while (!inStack.isEmpty()) {
            outStack.push(inStack.pop());
        }
    }
}
