package helpers.string;

public class EncodedChar {

    private int count;
    private char character;

    public EncodedChar(char character, int count) {
        this.count = count;
        this.character = character;
    }

    public int getCount() {
        return count;
    }

    public char getCharacter() {
        return character;
    }
}
