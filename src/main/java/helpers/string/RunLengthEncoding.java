package helpers.string;

import java.util.ArrayList;
import java.util.List;

public class RunLengthEncoding {

    public static List<EncodedChar> encodeString(String str){

        List<EncodedChar> encodedChars = new ArrayList<>();

        int len = str.length();

        for ( int i = 0; i < len; i++ ){

            int count = 1;
            while ( i < len - 1 ){
                if (str.charAt(i) != str.charAt(i+1)){
                    break;
                }
                count++;
                i++;
            }

            encodedChars.add(new EncodedChar(str.charAt(i), count));

        }

        return encodedChars;
    }


}
