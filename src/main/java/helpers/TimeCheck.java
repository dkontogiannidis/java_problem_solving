package helpers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TimeCheck {

    public static <T> void executeBatch(List<TimeChecked<T>> operations) {

        System.out.println("\n*****************************************************************************************");
        System.out.println("*** Starting execution of operations batch ");
        System.out.println("*****************************************************************************************");
        System.out.println(String.format("Timestamp: %s", LocalDateTime.now().toString()));
        System.out.println(String.format("Batch size: %d", operations.size()));
        System.out.println();

        TreeMap<Integer, StringBuilder> printMap = new TreeMap<>(Comparator.comparing(x -> x));
        ExecutorService es = Executors.newCachedThreadPool();
        final Object lock = new Object();

        for (int i = 0; i < operations.size(); i++) {
            final int finalI = i;

            es.execute(
                    () -> {
                        StringBuilder out = new StringBuilder();
                        out.append("\n*****************************************************************************************\n");
                        System.out.println(String.format(" -- %s - Thread %d started", LocalDateTime.now().toString(), finalI + 1));
                        out.append(String.format("*** Operation: %d \n", finalI + 1));
                        execute(operations.get(finalI), out);
                        synchronized ( lock ) {
                            printMap.put(finalI, out);
                        }
                        System.out.println(String.format(" -- %s - Thread %d finished", LocalDateTime.now().toString(), finalI +1));
                    });
        }
        es.shutdown();
        try {
            es.awaitTermination(2, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Integer i : printMap.navigableKeySet()) {
            System.out.print(printMap.get(i).toString());
        }
        System.out.println("\n*****************************************************************************************");
    }

    public static <T> void execute(TimeChecked<T> operation) {
        StringBuilder out = new StringBuilder();
        execute(operation, out);
        System.out.println(out.toString());
    }

    public static <T> void execute(TimeChecked<T> operation, StringBuilder out) {
        T res = null;
        String error = null;
        long start = System.nanoTime();
        try {
            res = operation.run();
        } catch (Exception e) {
            error = e.getMessage();
        }
        long diff = System.nanoTime() - start;

        out.append("*****************************************************************************************\nOutput: \n ");
        printResult(res, error, out);
        out.append(String.format("Elapsed time: \n %s ms \n %s ns ", TimeUnit.NANOSECONDS.toMillis(diff), diff));
    }

    private static <T> void printResult(T result, String error, StringBuilder out) {

        if (result == null)
            out.append(error);

        else if (result instanceof String)
            out.append(result);

        else if (result instanceof int[])
            out.append(Arrays.toString((int[]) result));

        else
            out.append(result.toString());

        out.append("\n");

    }



}
