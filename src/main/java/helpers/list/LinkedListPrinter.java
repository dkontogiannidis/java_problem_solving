package helpers.list;

public class LinkedListPrinter {

    public static void print(SinglyLinkedListNode node, String split) {
        if (node == null) {
            System.out.print("null");
            return;
        }
        System.out.print(node.data);
        while ((node = node.next) != null) {
            System.out.print(split + node.data);
        }
    }


}
