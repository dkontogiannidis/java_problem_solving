package helpers.list;

public class LinkedListUtils {

    public static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {
        SinglyLinkedListNode actualHead = head;
        if (head == null){
            return new SinglyLinkedListNode(data);
        }
        while(head.next != null) {
            head = head.next;
        }
        head.next = new SinglyLinkedListNode(data);
        return actualHead;
    }

    public static SinglyLinkedListNode deleteNode(SinglyLinkedListNode head, int position) {
        SinglyLinkedListNode actualHead = head;
        if (position == 0) {
            return actualHead.next;
        }
        for (int i = 1; i < position; i++){
            if (head != null && head.next != null){
                head = head.next;
            }
            else {
                return actualHead;
            }
        }
        head.next = head.next.next;
        return actualHead;
    }

}
