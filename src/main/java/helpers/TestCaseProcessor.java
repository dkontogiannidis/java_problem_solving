package helpers;

public interface TestCaseProcessor<T> {

    T process();

}
