package helpers;

public interface PrintingTimeChecked extends TimeChecked {
    void print();
}
