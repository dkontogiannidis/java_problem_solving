package helpers.tree;

public class Tree {

    Node head;

    public Tree( Node head ){
        this.head = head;
    }

    public static class Node {

        public int data;
        public Node left;
        public Node right;

        public Node(int data) {
            this.data = data;
        }

        public String toString(){
            return "Node -> "+ data;
        }
    }

}
