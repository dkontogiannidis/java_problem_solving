package problemRunner.input;

import hackerrank.stacksAndQueues.EqualStacks;
import org.junit.jupiter.api.Test;
import problemRunner.inputFile.InputFileResolver;

import java.io.File;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class InputFileResolverTest {

    @Test
    void testResolveFiles() throws Exception{
        File[] files = InputFileResolver.resolveFiles(EqualStacks.class);
        assertThat(Arrays.stream(files).map(File::getName)).contains("largeCase.txt");
    }
}